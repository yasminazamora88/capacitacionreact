/*
function suma(num1,num2){
    letsuma=num1+num2;
}

console.log("Suma: " + suma(1,1));

function factorial(n){
    let res = 1;
    for (let i = n; i >= 1; i--){
        res = res *  i;
    }

    return res;
}

const fac = factorial(5);
console.log("Factorial: " + fac);

function factorialR(n){
    if (n<=1) return 1;

    return n*factorial(n-1);
}

const facR = factorialR(5);
console.log("Factorial Recursivo: " + facR);

function sumR(num){
    if (num === 0){
        return 0;
    }else{
        return num + sumR(--num);
    }
}
console.log("Suma con Recursividad: " + sumR(4));

const data = [
    {
        name: "nombre 1",
        children: [
            {
                name: "nombre 1.1",
                children: [
                    {
                        name: "nombre 1.1.1"
                    },
                    {
                        name: "nombre 1.1.2"
                    }
                ]
            },
            {
                name: "nombre 1.2",
                children: [
                    {
                        name: "nombre 1.2.1"
                    },
                    {
                        name: "nombre 1.2.2"
                    }
                ]
            }
        ]
    },
    {
        name: "nombre 2",
        children: [
            {
                name: "nombre 2.1",
                children:[
                    {
                    nombre: "nombre 2.1.1"
                    },
                    {
                        nombre: "nombre 2.1.2"
                    }
                ]
            },
            {
                name: "nombre 2.2",
                children:[
                    {
                        name: "nombre 2.2.1"
                    },
                    {
                        name: "nombre 2.2.2"
                    }
                ]
            }
        ]
    }
]

function showName(data){
data.forEach(d => {
   if (d.name){
       console.log(d.name);
    }

    if (d.children){
       showName(d.children)
    }
});

}

showName(data);

function inicia() {
    let nombre = "Mozilla";

    function muestraNombre() {
        console.log(nombre);
    }

    muestraNombre();
}

inicia();
*/
/*


const clave = {a:2, b:1};
function operaciones(ope, clave){
    let oper;
    if (ope === "+"){
        oper = function sum(clave){
            return clave.a + clave.b;
        }

    }

    if (ope === "-"){
        oper = function rest(clave){
            return clave.a - clave.b;
        }
    }

    if (ope === "*"){
       oper =  function mult(clave){
           return clave.a * clave.b;
        }

    }

    if (ope === "/"){
        oper = function div(clave){
            if (clave.b>0)
                return clave.a / clave.b;
        }
    }

    return oper;
}



let op = operaciones("*",clave);

console.log(op);*/
/*
const saluda = () =>{
    console.log('Hola mundo')
}

const sumar = (a,b) => {
    return a + b
}

const sumar2 = (a,b) => a+b
*/

/*
Arrow Function

const clave = {a:2, b:1};
const op = (ope, clave) =>{
    var oper;
    if (ope === "+"){
       return sum = () => {
             clave.a + clave.b
        }
    }

    if (ope === "-"){
        return resta = () => {
            clave.a - clave.b
        }
    }

    if (ope === "*"){
        return mult = () => {
            clave.a * clave.b
        }

    }

    if (ope === "/"){
        return div = () => {
            if (clave.b != 0) {
                clave.a / clave.b
            }
        }
    }
}

var suma = op('/', clave);
console.log(suma);
 */

//Modules
/*
suma.js
module.exports = data => data.a + data.b;
 */

/*
index.js
const sumar = require("./suma.js");
const resultado = sumar ({a:2, b:3});

console.log(resultado);
 */

//Modules ES6+
/*
suma.js
const sumar = data => data.a + data.b

export default sumar; //no puede haber mas de uno en cada fichero
 */

/*
index.js

import sumar from "./suma.js";

const resultado = sumar({a:2, b:3});

console.log(resultado);
 */